import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(tasks: any, term?: any): any {
  //Check if search term is undefined
  if (term === undefined) return tasks;
  //return updated tasks array
  return tasks.filter(function(task){
    return task.title.toLowerCase().includes(term.toLowerCase());
  })
  }

}
