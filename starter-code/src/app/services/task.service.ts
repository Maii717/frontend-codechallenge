import { Injectable,OnInit } from '@angular/core';

import {Http, Response} from "@angular/http";




import 'rxjs/add/operator/map';
@Injectable()
export class TaskService {

  private apiUrl = 'https://jsonplaceholder.typicode.com/todos';
  data: any = {};

  constructor(public http: Http) {
   console.log("servicio tarea LISTO");
  }




   getTasks(term: string){
     return this.http.get(this.apiUrl)
     .map((res: Response) => res.json());

   }

   addTask(task){
     return this.http.post(this.apiUrl,task)
     .map(res => res.json());

   }

   deleteTask( id:number){

     return this.http.delete(this.apiUrl+'/'+ id)
           .map(res => res.json);

   }




   }
