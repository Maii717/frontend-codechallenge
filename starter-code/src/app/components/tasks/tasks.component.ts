import { Component, OnInit } from '@angular/core';
import { TaskService} from "../../services/task.service";
import { ActivatedRoute } from '@angular/router';



@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styles: [],
  providers: [TaskService],


})
export class TasksComponent {

  public data: any;
  id:number;
  term: string = '';
  task={
    title:"",
    completed:false
  }

  constructor(public _task: TaskService) {

        this._task.getTasks(this.term)
         .subscribe(data=> {
            console.log(data);
            this.data = data

      });
  }

  onSubmit(){
    this._task.addTask(this.task)
    .subscribe(task => {
      this.data.unshift(this.task);
      console.log(this.data)
    })
  }

   delete(){
     this._task.deleteTask(this.task.id)
     .subscribe(task => {
       console.log(this.task.id)
     });
     })
   }
}
