import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {app_routing} from "./app.routes";

import {HttpModule} from "@angular/http";

import {FormsModule} from "@angular/forms";

//Services
import{ TaskService } from "./services/task.service";

//Pipes
import { FilterPipe} from "./pipes/filter.pipe"

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { TasksComponent } from './components/tasks/tasks.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TasksComponent,
    NavbarComponent,
    FilterPipe

  ],
  imports: [
    BrowserModule,
    app_routing,
    HttpModule,
    FormsModule
  ],
  providers: [TaskService],
  bootstrap: [AppComponent],
})
export class AppModule { }
